from django import template
from ..util import localization

register = template.Library()


@register.simple_tag(takes_context=True)
def localize(context, param):
    return localization.localize(context['request'], param)
