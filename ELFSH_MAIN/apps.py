from django.apps import AppConfig


class ElfshMainConfig(AppConfig):
    name = 'ELFSH_MAIN'
