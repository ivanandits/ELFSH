from . import *

create_house_schema = DictSchema(
    {
        "captcha_id": StringSchema(regex=r"^[0-9a-fA-F\-]+$"),
        "name": StringSchema(regex=".+", message="validation_house_name"),
        "captcha": StringSchema(regex=".+", message="validation_captcha"),
    },
    {
        'userinfo': DictSchema(
            {
                "username": StringSchema(regex=".+", message="validation_username"),
                "password": NewPasswordSchema(regex=".+", message="validation_password"),
                "email": StringSchema(regex=".+@.+", message="validation_email"),
            },
        ),
    }
)

validate_email_schema = DictSchema(
    {
    }
)
