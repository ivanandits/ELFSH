from django.contrib.auth import password_validation

import re

class _BasicSchema(object):
    def validate(self, obj, name=None):
        """
        Validates the given object under the schema
        Returns None if valid, and a string message if invalid
        :param obj: The object to validate
        :param name: used to format messages
        :return: The error message
        """
        return None

    def convert(self, obj):
        return obj


class ListSchema(_BasicSchema):
    def __init__(self, itemtype, name=None):
        self.itemtype = itemtype
        self.name = name

    def validate(self, obj, name=None):
        name = self.name or name
        if not isinstance(obj, list):
            return "Object " + repr(self.name) + " must be a list"
        for i in obj:
            val = self.itemtype.validate(i, name=str(name) + " item")
            if val:
                return val
        return None


class DictSchema(_BasicSchema):
    def __init__(self, required_params, optional_params={}, message="Key {} not found", name=""):
        self.requiredParams = required_params
        self.optionalParams = optional_params
        self.message = message
        self.name = name

    def validate(self, obj, name=""):
        if not isinstance(obj, dict):
            return self.message or "Object " + repr(self.name) + " be of type dict"
        for key, validator in self.requiredParams.items():
            if key not in obj or validator.validate(obj[key]):
                if key not in obj:
                    return self.message.format(key)
                else:
                    return validator.validate(obj[key], name=key)
        for key, validator in self.optionalParams.items():
            if key in obj and validator.validate(obj[key]):
                return validator.validate(obj[key], name=key)
        return None


class FloatSchema(_BasicSchema):
    def __init__(self, message="", min=None, max=None, name=""):
        self.message = message
        self.name = name
        self.min = min
        self.max = max

    def validate(self, obj, name=""):
        name = name or self.name
        if not (isinstance(obj, float) or isinstance(obj, int)):
            return name + " should be a number"
        if (self.min is not None and obj < self.min) or (self.max is not None and obj > self.max):
            return self.message or self.name + " should be between " + repr(self.min) + " and " + repr(self.max)

    def convert(self, obj):
        return float(obj)


class IntSchema(_BasicSchema):
    def __init__(self, min=None, max=None, message="", name=""):
        self.message = message
        self.name = name
        self.min = min
        self.max = max

    def validate(self, obj, name=""):
        name = name or self.name
        if not isinstance(obj, int):
            return self.message or name + " should be a integer"
        if (self.min is not None and obj < self.min) or (self.max is not None and obj > self.max):
            return self.message or self.name + " should be between " + repr(self.min) + " and " + repr(self.max)


class StringSchema(_BasicSchema):
    def __init__(self, regex="", message="", name=""):
        self.message = message
        self.name = name
        self.regex = re.compile(regex)

    def validate(self, obj, name=""):
        name = name or self.name
        if not isinstance(obj, str) or (self.regex and not self.regex.match(obj)):
            return self.message or name + " should be a string"


class BoolSchema(_BasicSchema):
    def __init__(self, regex="", message="", name=""):
        self.message = message
        self.name = name
        self.regex = re.compile(regex)

    def validate(self, obj, name=""):
        name = name or self.name
        if not isinstance(obj, bool):
            return self.message or name + " should be a string"


class NewPasswordSchema(StringSchema):
    def validate(self, obj, name=""):
        res = super().validate(obj, name)
        if res:
            return res

        try:
            password_validation.validate_password(obj)
        except password_validation.ValidationError as ex:
            return str(ex)
