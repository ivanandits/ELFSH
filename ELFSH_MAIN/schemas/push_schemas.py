from . import *

"""("registration_id", str),
            ("p256dh", str),
            ("auth", str),
            ("browser", str)"""

register_push_schema = DictSchema(
    {
        "registration_id": StringSchema(),
        "auth": StringSchema(regex=r".{24}"),
        "p256dh": StringSchema(regex=r".{88}"),
        "browser": StringSchema(regex=".*")
    }
)