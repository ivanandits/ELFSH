import React from 'react';
import {Link} from 'react-router-dom';
import {localize} from '../util/localization.js';
import {enablePushNotifications} from "../notifications/util";
import {getLanguage} from "../util/localization";

export class Userinfo extends React.Component {
  componentDidMount() {
    fetch("/api/current_user_info",
      {
        "credentials": "same-origin"
      }
    ).then(
      (response) => {
        if (response.status === 403) {
          this.props.onUserChange(false, undefined);
        }
        else {
          response.json().then(data => {
            this.props.onUserChange(true, data)
          });
        }
      }
    )
  }

  render() {
    if (!this.props.loggedIn) {
      return null;
    }

    return <div className="header">
      <span>{localize("header_welcome")}{this.props.user.username}</span>
      <a onClick={(event) => {
        event.preventDefault();
        enablePushNotifications()
      }} href="">{localize("header_notify")}</a>
      <Link to={`/${getLanguage()}/logout`} id={"logout-button"}>
        {localize("header_logout")}
      </Link>
    </div>
  }
}