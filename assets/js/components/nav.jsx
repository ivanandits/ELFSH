import React from 'react';
import {Link} from 'react-router-dom';

import {localize} from '../util/localization.js';

export class Nav extends React.Component {
  render() {
    return <div className="nav">
      <div className={"nav-element " + (this.props.selected === "lijst" ? "nav-element-selected" : "")}
           id={'nav-list-div'}>
        <Link to={`/${this.props.match.params.lang}/list/${this.props.match.params.house_id}`}
              id={'nav-list'}>
          {localize("nav_list")}
        </Link>
      </div>

      <div className={"nav-element " + (this.props.selected === "cost" ? "nav-element-selected" : "")}
           id={'nav-cost-div'}>
        <Link to={`/${this.props.match.params.lang}/cost/${this.props.match.params.house_id}`}
              id={'nav-cost'}>
          {localize("nav_cost")}
        </Link>
      </div>

      <div className={"nav-element " + (this.props.selected === "settings" ? "nav-element-selected" : "")}
           id={'nav-settings-div'}
      >
        <Link to={`/${this.props.match.params.lang}/settings/${this.props.match.params.house_id}`}
              id={'nav-settings'}>
          {localize("nav_settings")}
        </Link>
      </div>
    </div>
  }
}