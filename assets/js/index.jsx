/* Current links: 16 */
/* this file is bootstrapping code */
import React from 'react';
import ReactDOM from 'react-dom';

/* Stuff for font awesome*/
import {library} from '@fortawesome/fontawesome-svg-core';
import {
  faStar,
  faEdit,
  faTrashAlt,
  faEye,
  faUtensils,
  faCircleNotch,
  faCaretRight,
  faCaretDown,
  faCheck,
  faTimes,
  faExclamationTriangle
} from '@fortawesome/free-solid-svg-icons';
import {faGitlab} from '@fortawesome/free-brands-svg-icons';

library.add(
  faStar,
  faEdit,
  faTrashAlt, faEye, faUtensils, faGitlab, faCircleNotch, faCaretRight, faCaretDown, faCheck, faTimes,
  faExclamationTriangle);

import {set_cookie, get_cookie} from './util/cookies.js';
import {registerServiceWorker} from "./notifications/notifications";

registerServiceWorker();

if (!get_cookie("lang")) {
  set_cookie("lang", getDefaultLanguage());
}
if (!localStorage.getItem("lang")) {
  localStorage.setItem("lang", get_cookie("lang"))
}

/*stuff for react*/
//import App from external library
import {App} from './app.jsx';
import {getDefaultLanguage} from "./util/localization";

ReactDOM.render(<App/>, document.getElementById('container'));