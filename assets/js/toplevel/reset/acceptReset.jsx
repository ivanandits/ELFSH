import React from "react";
import {localize} from "../../util/localization";
import {request} from "../../util/request";
import {ErrorView} from "../../components/error";

export default class AcceptResetPage extends React.Component {
  constructor() {
    super();

    this.state = {
      "password": "",
      "password2": "",
      "loading": false,
      "error": undefined,
    }
  }

  componentDidMount() {
    document.title = localize('page_reset') + ' - ELFSH';
    request(
      "/api/reset/verify/" + this.props.match.params.code
    ).catch(
      (error) => this.setState({error: error})
    )
  }

  submit(event) {
    event.preventDefault();

    this.setState(
      {
        loading: true
      }
    );

    if (this.state.password !== this.state.password2) {
      this.setState(
        {"error": "Passwords do not match"}
      );
      return;
    }


    request(
      "/api/reset/apply/" + this.props.match.params.code,
      {
        "password": this.state.password
      }
    ).then(
      () => this.props.history.push(
        `/${this.props.match.params.lang}/login`
      )
    ).catch(
      (error) => {
        this.setState(
          {
            "error": error
          }
        )
      }
    )
  }

  render() {
    return <form>
      <ErrorView error={this.state.error}/>
      <label className="inputLabel">
        New password:
        <input type="password" value={this.state.password}
               onChange={(event) => this.setState({password: event.target.value})}/>
      </label>
      <label className="inputLabel">
        Confirm password:
        <input type="password" value={this.state.password2}
               onChange={(event) => this.setState({password2: event.target.value})}/>
      </label>

      <button onClick={this.submit.bind(this)}>{localize("cost_submit")}</button>
    </form>
  }
}