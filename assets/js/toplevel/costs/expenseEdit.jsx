import React from "react";
import {formatMoney, localize} from "../../util/localization";
import {submitExpenseChange} from "./util";
import {ErrorView} from "../../components/error";
import {MoneyInput} from "../../generic/numberInput";
import {Dropdown} from "../../generic/dropdown";

class ExpensePerson extends React.Component {
  render() {
    return <div className={"expense-person" + (this.props.value === 0 ? " expense-person-gray" : "")}>
      <div className={"table-elem table-elem-add"}
           onClick={() => this.props.onChange(Math.max(this.props.value - 1, 0))}>-
      </div>
      <div className="expense-person-middle">{this.props.person}
        <span className={this.props.value === 0 ? "" : "green"}>×{this.props.value}</span>
      </div>
      <div className={"table-elem table-elem-add"} onClick={() => this.props.onChange(this.props.value + 1)}>+
      </div>
    </div>

    /*<label>
                        <input type="checkbox"
                               checked={this.props.value!==0}
                               onChange={event=>this.props.onChange(event.target.checked?1:0)}
                               />
                        {this.props.person}
                    </label>*/
  }
}

export class ExpenseEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "error": undefined,
      "loading": false
    }
  }

  submitNewCost(event) {
    event.preventDefault();
    submitExpenseChange(
      this.props.house_id,
      0,
      this.props.params.description,
      this.props.params.amount,
      this.props.params.paid_by,
      this.props.params.payed_for,
      false,
      this.props.maxEntries,
      this.props.params.is_meal
    ).then(
      (data) => {
        this.props.replaceData(data);
        this.setState({"error": undefined});
        this.props.onChange("description", "");
        this.props.onChange("amount", "");
        window.scroll(0, 0);
      }
    ).catch(
      (error) => {
        console.log(error);
        this.setState({"error": error});
      }
    )
  }

  render() {
    return <form>
      <h2>{localize("cost_add_expense")}</h2>
      <ErrorView error={this.state.error}/>
      <label className="inputLabel">{localize("cost_description")}
        <input value={this.props.params.description}
               onChange={(event) => this.props.onChange("description", event.target.value)}/>
      </label><br/>
      <label className="inputLabel">{localize("cost_amount")}
        <MoneyInput
          value={this.props.params.amount}
          onChange={value => this.props.onChange("amount", value)}
        />
      </label><br/>
      <label className="inputLabel">{localize("cost_paid_by")}
        <Dropdown value={this.props.params.paid_by}
                  options={this.props.user_info.map(
                    i => ({
                      "name": i.name + " (" + formatMoney(i.value) + ")",
                      "value": i["id"]
                    }))}
                  onChange={value => this.props.onChange("paid_by", value)}
        />
      </label><br/>
      <span>{localize("cost_payed_for")}</span>
      {
        Object.keys(this.props.params.payed_for).map(
          (key) => (
            <ExpensePerson key={key} person={key} value={this.props.params.payed_for[key]}
                           onChange={(value) => {
                             this.props.onChange(
                               "payed_for", {...this.props.params.payed_for, [key]: value}
                             )
                           }
                           }/>
          )
        )
      }
      <br/>
      <button
        onClick={this.submitNewCost.bind(this)}>{localize("cost_submit")}
      </button>
    </form>
  }
}