import React from 'react';
import {localize} from "../util/localization";
import {Link} from "react-router-dom";

export default class ErrorPage extends React.PureComponent {
  componentDidMount() {
    document.title = "404 - ELFSH - "+localize('page_main')
  }

  render() {
    return <>
      <h1>{localize('404_title')}</h1>

      <p className={'text-container'}>
        {localize('404_prelink')}
      </p><br/>
      <Link to={'/'} className='button'>{localize('404_link')}</Link><p/>
      <p className={'text-container'}>
        {localize('404_postlink')}
      </p>
    </>
  }
}