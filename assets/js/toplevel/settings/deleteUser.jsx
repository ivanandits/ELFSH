import React from 'react';
import {Dropdown} from "../../generic/dropdown";

export class DeleteUserScreen extends React.Component {
  render() {
    return <>
      <h1>Are you sure you want to delete {this.props.name}</h1>
      <label>
        <Dropdown/>
      </label>
      <label>
        <Dropdown/>
      </label>
      <button>Ok</button>
      <button>Cancel</button>

      </>
  }
}