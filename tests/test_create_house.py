import json

from .test_views import BasicTestCase
from ELFSH_MAIN import models
from django.core import mail


class CreateHouseTest(BasicTestCase):
    captcha_value = "TEST CAPTCHA FOR TESTING PURPOSES"

    def create_valid_captcha(self, value=captcha_value):
        captcha = models.CaptchaRecord(
            value=value
        )
        captcha.save()
        print(str(captcha.id))
        return str(captcha.id)

    def testAllOkLoggedIn(self):
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest"
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200,
                         response.content
                         )

        body = json.loads(
            response.content
        )

        house_id = body["house_id"]

        # Make sure all these objects exist
        house = models.House.objects.get(id=house_id)
        elfsh_user = models.ELFSHUser.objects.get(user_id=body["user_id"])
        membership = models.HouseMembership.objects.get(
            house=house,
            user=elfsh_user
        )
    def testAllOkLoggedOut(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": "this_username_does_not_exist",
                        "password": "1R5B84*3bb",
                        "email": "testValidName@elfsh.mousetail.nl"
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200,
                         response.content
                         )

        self.assertEquals(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEquals(email.to, ['testValidName@elfsh.mousetail.nl'])
        link = max(email.body.split('\n'), key=lambda i: i.count('/'))
        code = link.split("/")[-1]
        self.assertGreaterEqual(len(code), 20)

        response = self.client.post(
            '/api/verify/' + code
        )

        self.assertEquals(response.status_code, 200, response.content)

        body = json.loads(
            response.content
        )

        user_info = self.client.get(
            '/api/current_user_info'
        )
        try:
            user_info = json.loads(
                user_info.content
            )
        except json.decoder.JSONDecodeError:
            self.fail(user_info.content)

        self.assertEquals(body["user_id"], user_info["id"])

        # Make sure all these objects exist
        house = models.House.objects.get(id=body["house_id"])
        elfsh_user = models.ELFSHUser.objects.get(user_id=body["user_id"])
        membership = models.HouseMembership.objects.get(
            house=house,
            user=elfsh_user
        )

    def testInvalidCaptcha(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value[:-1] + '!',
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": "this_username_does_not_exist",
                        "password": "1R5B84*3bb",
                        "email": "testInvalidName@elfsh.mousetail.nl"
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )

    def testNonexistantCaptcha(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha()[:-1] + '?',
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": "this_username_does_not_exist",
                        "password": "1R5B84*3bb",
                        "email": "testInvalidName@elfsh.mousetail.nl"
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )

    def testExistingUsername(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": self.users[0].user.username,
                        "password": "1R5B84*3bb",
                        "email": "testInvalidName@elfsh.mousetail.nl"
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )

    def testEmptyUsername(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": '',
                        "password": "1R5B84*3bb",
                        "email": "testInvalidName@elfsh.mousetail.nl"
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )

    def testEmptyHouseName(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "",
                    "userinfo": {
                        "username": 'create_house_test',
                        "password": "1R5B84*3bb",
                        "email": "testInvalidName@elfsh.mousetail.nl"
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )

    def testExistingEmail(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": 'create_house_test',
                        "password": "1R5B84*3bb",
                        "email": self.users[0].user.email
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )

    def testBadPassword(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": 'create_house_test',
                        "password": "password",
                        "email": 'create_house_test@elfsh.mousetail.nl'
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )

    def testBadEmail(self):
        self.client.logout()
        response = self.client.post(
            '/api/new_house',
            json.dumps(
                {
                    "captcha_id": self.create_valid_captcha(),
                    "captcha": self.captcha_value,
                    "name": "CreateHouseTest",
                    "userinfo": {
                        "username": 'create_house_test',
                        "password": "1V%c12B86c{",
                        "email": 'create_house_test_elfsh.mousetail.nl'
                    }
                }
            ),
            content_type="application/json"
        )
        self.assertEqual(
            response.status_code, 400,
            response.content
        )
