from . import frontend_test


class TestEatStateFrontend(frontend_test.FrontendTest):
    def testBasicLoginStuff(self):
        self.client.logout()

        self.browser.get(self.live_server_url)
        self.waitForElement('#username').send_keys(
            self.users[0].user.username
        )
        self.browser.find_element_by_id('password').send_keys(
            self.passwords[0]
        )
        self.browser.find_element_by_css_selector('button[type=submit]').click()

        table = self.waitForElement('table')
        elements = table.find_elements_by_class_name(
            'table-elem-0'
        )
        length = len(elements)

        elements[0].click()

        self.browser.find_element_by_id('nav-settings').click()
        self.waitForElement('#nav-settings-div.nav-element-selected')
        self.browser.find_element_by_id('nav-list').click()

        table = self.waitForElement('table')
        elements = table.find_elements_by_class_name('table-elem-0')
        self.assertEqual(len(elements), length - 1)
