from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from .test_views import BasicTestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class FrontendTest(BasicTestCase, StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = FirefoxOptions()
        #options.headless = True
        cls.browser = webdriver.Firefox(
            firefox_options=options
        )
        cls.browser.implicitly_wait(10)

    def waitForElement(self, selector):
        return WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, selector))
        )

    def setUp(self):
        super().setUp()
        self.client.logout()
        self.browser.get(self.live_server_url)

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()
