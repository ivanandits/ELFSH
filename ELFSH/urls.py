"""ELFSH URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
#import django.conf.urls as urls
from django.contrib import admin

from ELFSH_MAIN import urls
from ELFSH_MAIN.views import errorviews

urlpatterns = [
    url(r'^admin/', admin.site.urls),

] + urls.urlpatterns

handler400 = errorviews.handler400
handler403 = errorviews.handler403
handler404 = errorviews.handler404
handler500 = errorviews.handler500
